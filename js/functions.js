var loaded = false;
//var watchScrolling = true;
//window.onscroll = function() {scrolling()};

function menuToggle(x) {

    x.classList.toggle( "change" ); //css to animate hamburger
    jQuery( "#modal" ).toggle();

    if (!loaded) { //first time need to give a width
      jQuery( "#popout" ).animate({ width: "250px" }, 300 );
        loaded = true;
        setTimeout(appearQuotes, 200);
        return;
    };

    jQuery("#popout").animate({ //after first run a toggle event works
      width: "toggle"
    });

    //wait till the sidebar comes out to display quote
    jQuery("#menu-toggle").hasClass("change") ? setTimeout(appearQuotes, 200) : jQuery("#quotes").toggle();

    function appearQuotes() {
      jQuery("#quotes").fadeIn(800);
    }

}


function scrolling() { //closes the menu when the user starts scrolling

    if(watchScrolling==false) return;

    if (jQuery("#popout").width()==0 || jQuery("#popout").css("display")=="none") return;

    watchScrolling = false;

    menuToggle(document.getElementById("menu-toggle"));
    setTimeout(waiting, 400); //needs a bit of time otherwise onscroll triggers multipule times

    function waiting() {
            watchScrolling = true;
    }
}
