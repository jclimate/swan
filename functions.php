<?php

function enqueue_styles() {
   wp_enqueue_style( 'style', get_template_directory_uri().'/style.css' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_styles' );

function register_my_menu() {
  register_nav_menu('popout-menu',__( 'Popout Menu' ));
  register_nav_menu( 'social-media', _x( 'Social Media', 'nav menu location', 'example-textdomain' ) );
}
add_action( 'init', 'register_my_menu' );


function wp_add_jsfunctions() {
    wp_register_script('my_js_functions', get_theme_root_uri() . '/swan-theme/js/functions.js');
    wp_enqueue_script('my_js_functions');
}
add_action ('wp_enqueue_scripts', 'wp_add_jsfunctions');

add_action( 'init', 'my_register_menus' );

function my_register_menus() {
	register_nav_menu( 'social-media', _x( 'Social Media', 'nav menu location', 'example-textdomain' ) );
}

/**
 * Register widgetized areas.
 *
 */
function contact_widgets_init() {

	register_sidebar( array(
		'name'          => 'Footer Contact Info',
		'id'            => 'contact_info',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'contact_widgets_init' );

function quotes_widgets_init() {

	register_sidebar( array(
		'name'          => 'Quotes Widget',
		'id'            => 'quotes',
		'before_widget' => '<div id="quotes">',
		'after_widget'  => '</div>',
	) );

}
add_action( 'widgets_init', 'quotes_widgets_init' );
