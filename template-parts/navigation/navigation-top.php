<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'twentyseventeen' ); ?>">

<?php


		echo '<a id="menu-link" style="position: absolute;" href="http://swan.climatejourney.com"><div id="menu-logo"><image id="da-logo" src="http://swan.climatejourney.com/wp-content/uploads/2018/06/swanlogo.png"></div></a>';

?>

 <div class="menu-toggle container" onclick="menuToggle(this)">
  <div class="bar1"></div>
  <div class="bar2"></div>
  <div class="bar3"></div>
</div>

    <a href="http://swan.climatejourney.com/donate"><div id="menu-right">DONATE</div></a>

<div id="toggle"><img src="http://www.example.com/wp-content/themes/your-theme/images/menu.png" alt="Show" /></div>
<div id="popout">
<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
</div>


<?php

wp_nav_menu( array(
		'theme_location' => 'top',
		'menu_id'        => 'top-menu',
	) ); ?>

	<?php


if ( ( twentyseventeen_is_frontpage() || ( is_home() && is_front_page() ) ) && has_custom_header() ) : ?>
		<a href="#content" class="menu-scroll-down"><?php echo twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ); ?><span class="screen-reader-text"><?php _e( 'Scroll down to content', 'twentyseventeen' ); ?></span></a>
	<?php endif; ?>
</nav><!-- #site-navigation -->
