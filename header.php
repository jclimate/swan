<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
    <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>  onscroll="scrolling()">
    <div id="page" class="site">

	<header id="masthead" class="site-header" role="banner">
        <div class="navigation-top">
            <div id="top-center">


        <div id="menu-toggle" class="menu-toggle container" onclick="menuToggle(this)">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
        </div>



        <a id="menu-link" href="<?php echo get_site_url(); ?>"><div id="menu-logo"><image id="da-logo" src="<?php echo get_site_url(); ?>/wp-content/uploads/2018/06/swanlogo.png"></div></a>

        <a href="<?php echo get_site_url(); ?>/donate/"><div id="menu-right">DONATE</div></a>

       </div>
	        </div>

	</header><!-- #masthead -->

	<div class="site-content-contain">

		<div id="content" class="site-content">

            <div id="popout">

<?php wp_nav_menu( array( 'theme_location' => 'popout-menu' ) ); ?>

<?php if ( is_active_sidebar( 'quotes' ) ) : ?>
    <?php dynamic_sidebar( 'quotes' ); ?>
<?php endif; ?>

</div>
<div id="modal" onclick="menuToggle(document.getElementById('menu-toggle'))">
</div>
