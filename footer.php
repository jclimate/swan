<?php
/*te for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage 
 * @since 1.0
 * @version 1.2
 */

?>

        </div><!-- #content -->

        <footer id="colophon" class="site-footer" role="contentinfo">
            <div class="wrap">
              <?php if ( has_nav_menu( 'social-media' ) ) {

              	wp_nav_menu(
              		array(
              			'theme_location'  => 'social-media',
              			'container'       => 'nav',
              			'container_id'    => 'menu-social-media',
              			'container_class' => 'menu',
              			'menu_id'         => 'menu-social-media-items',
              			'menu_class'      => 'menu-items',
              			'depth'           => 1,
              			'fallback_cb'     => '',
              		)
              	);
              } ?>

              <?php if ( is_active_sidebar( 'contact_info' ) ) : ?>
              		<?php dynamic_sidebar( 'contact_info' ); ?>
              <?php endif; ?>


            </div><!-- .wrap -->
        </footer><!-- #colophon -->
    </div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
